FROM node:14.15-stretch AS builder

WORKDIR /app

COPY package.json .
RUN npm install

COPY . .
RUN npm run build --prod

FROM nginx:alpine

COPY --from=builder /app/public/* /usr/share/nginx/html/
COPY nginx.conf /etc/nginx/conf.d/default.conf